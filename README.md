# HL7 FHIR R4B Terminology

This package repackages all terminology resources (CodeSystem, ValueSet, ConceptMap) from the FHIR R4B release, available from http://hl7.org/fhir/R4B/hl7.fhir.r4b.core.tgz for distribution via SU-TermServ's terminology server.

## Known Issues

Please also refer to https://gitlab.com/mii-termserv/fhir-resources/hl7.terminology/-/blob/main/README.md, as the FHIR R4B package depends extensively on the THO (terminology.hl7.org) package.

### Changed resources from base definition

- http://hl7.org/fhir/therapy-relationship-type (changed first instance of indicated-only-before to indicated-only-after, to bring this in line with https://jira.hl7.org/browse/FHIR-37450 and the changed definition in FHIR R5)
- http://terminology.hl7.org/CodeSystem/dicom-audit-lifecycle (deleted the CodeSystem from this package as it is provided in THO, and including it here results in errors during expansion. The list of concepts defined by both versions are to a high degree of confidence identical.)
    - same with http://terminology.hl7.org/CodeSystem/service-type
    - same with http://terminology.hl7.org/CodeSystem/appointment-cancellation-reason
    - same with http://terminology.hl7.org/CodeSystem/iso-21089-lifecycle
    - same with http://terminology.hl7.org/CodeSystem/program
    - same with http://terminology.hl7.org/CodeSystem/service-category
- http://hl7.org/fhir/ValueSet/doc-typecodes: For Ontoserver the definition requires the LOINC Part codes for the filter `SCALE_TYP`. This is similar to https://github.com/medizininformatik-initiative/kerndatensatzmodul-mikrobiologie/issues/15. The VS was changed to filter using the LOINC Part code `LP32888-7` instead of the string `Doc`.
- http://hl7.org/fhir/ValueSet/task-code: The version `3.6.0` of the CS referenced was removed to bring it in-line with the R5 definition.
- http://hl7.org/fhir/ValueSet/security-labels: The imported ValueSet http://terminology.hl7.org/ValueSet/v3-ConfidentialityClassification is now named http://terminology.hl7.org/ValueSet/v3-Confidentiality. In R5, this change was already made, so the change was backported.
- http://hl7.org/fhir/ValueSet/endpoint-payload-type: The referenced OID-based CS (`urn:oid:1.3.6.1.4.1.19376.1.2.3`) is the IHE Format Code system. While this is an example VS, the definition was changed to the URL of the IHE ValueSet: http://ihe.net/fhir/ihe.formatcode.fhir/CodeSystem/formatcode

### Missing CodeSystems that are not easily available

- `urn:ietf:bcp:13` (Media Types, e.g. `application/fhir+json;utf-8`)
- `urn:ietf:bcp:47` (language codes, e.g. `de-DE`)
- `urn:iso:std:iso:3166` (countries, e.g. `DE` or `DEU`)
- `urn:iso:std:iso:4217` (currencies, e.g. `EUR`)
- https://precision.fda.gov/apps/
- http://www.ensembl.org
- https://precision.fda.gov/files/
- http://unitsofmeasure.org
- https://www.iana.org/time-zones
- http://www.ncbi.nlm.nih.gov/clinvar
- http://hl7.org/fhir/sid/icd-10
- http://hl7.org/fhir/ValueSet/allelename
- http://www.ama-assn.org/go/cpt
- http://hl7.org/fhir/sid/cvx
- http://www.nlm.nih.gov/research/umls/rxnorm
- http://nucc.org/provider-taxonomy
- http://www.genenames.org

### CodeSystems that are empty

- http://hl7.org/fhir/message-events (special CodeSystem that has an empty definition: "This Code System is normative - it is generated based on the information defined in this specification. The definition will remain fixed across versions, but the actual contents will change from version to version")
    - this also affects the ValueSet http://hl7.org/fhir/ValueSet/message-events

### Other types of CodeSystem issues

- http://hl7.org/fhir/CodeSystem/example-supplement: This is a CodeSystem supplement, and direct lookups to this CS are not supported. This is a non-issue.
- http://hl7.org/fhir/ValueSet/example-filter: There is a filter that is incorrectly used by the underlying CodeSystem, but this is an example resource from the FHIR standard and doesn't need to be fixed. Error: "Filter of property acme-plasma using operator 'EQUAL' is not supported by code system http://hl7.org/fhir/CodeSystem/example"
- http://ihe.net/fhir/ihe.formatcode.fhir/CodeSystem/formatcode

## ValueSets that don't expand

- http://hl7.org/fhir/ValueSet/insuranceplan-type due to the CodeSystem not being provided in the THO package
- http://hl7.org/fhir/ValueSet/allergen-class: The referenced SNOMED CT concept is inactive and has no children. It is only used as an example binding for `NutritionProduct.knownAllergen`, so a fix is not urgent/needed.
- http://hl7.org/fhir/ValueSet/entformula-type: The referenced concepts are from the US Extension of SNOMED CT. The VS is used as an example in `NutritionOrder.enteralFormula.baseFormulaType`. Uploading this extension of SNOMED CT only for this VS is nt neccessary, so WONTFIX.
    - same with http://hl7.org/fhir/ValueSet/texture-code
    - same with http://hl7.org/fhir/ValueSet/supplement-type
- http://hl7.org/fhir/ValueSet/specimen-collection-priority: A usable code system with URL http://example.com could not be resolved. The VS was changed significantly in the FHIR Extensions Pack: https://hl7.org/fhir/extensions/ValueSet-specimen-collection-priority.html, but the VS will not be updated.
- http://hl7.org/fhir/ValueSet/example-extensional: This VS references an antique version of LOINC, but is an example, so WONTFIX.
    - same with http://hl7.org/fhir/ValueSet/example-intensional
- http://hl7.org/fhir/ValueSet/consistency-type

